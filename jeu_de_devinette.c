#include <stdio.h>
#include <stdlib.h>
#include <time.h>

int main() {
    int number, guess, attempts = 0;
    
    // Générer un nombre aléatoire entre 1 et 100
    srand(time(NULL));
    number = rand() % 100 + 1;

    printf("Bienvenue dans le jeu de devinettes !\n");
    
    do {
        printf("Devinez le nombre (entre 1 et 100) : ");
        
        if (scanf("%d", &guess) != 1) {
            // Si la lecture échoue, effacer le tampon d'entrée
            while (getchar() != '\n');
            printf("Veuillez entrer un nombre valide.\n");
            continue;
        }
        
        attempts++;
        
        if (guess > number) {
            printf("Trop grand !\n");
        } else if (guess < number) {
            printf("Trop petit !\n");
        } else {
            printf("Félicitations ! Vous avez deviné le nombre en %d tentatives.\n", attempts);
        }
    } while (guess != number);
    
    return 0;
}
